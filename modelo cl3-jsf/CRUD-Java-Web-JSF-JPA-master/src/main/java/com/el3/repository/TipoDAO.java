package com.el3.repository;

import java.util.List;

import com.el3.entity.Tipo;

public interface TipoDAO {
	public List<Tipo> listar();
}
