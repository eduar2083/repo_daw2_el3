package com.el3.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "mercaderia")
public class Mercaderia implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idmercaderia")
	private int idMercaderia;
	
	@Column(name = "nombre")
	private String nombre;
	
	@Column(name = "stock")
	private int stock;
	
	@Column(name = "precio")
	private double precio;
	
	@JoinColumn(name = "idtipo")
	@ManyToOne
	private Tipo tipo;
	
	public Mercaderia() {
		tipo = new Tipo();
	}

	public int getIdMercaderia() {
		return idMercaderia;
	}

	public void setIdMercaderia(int idMercaderia) {
		this.idMercaderia = idMercaderia;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public Tipo getTipo() {
		return tipo;
	}

	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}
}
