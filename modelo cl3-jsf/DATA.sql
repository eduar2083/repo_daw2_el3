DROP DATABASE IF EXISTS bd_el3;
CREATE DATABASE bd_el3;
USE bd_el3;

CREATE TABLE tipo
(
	idtipo int not null auto_increment,
    nombre varchar(45) not null,
    
    constraint pk_tipo primary key (idtipo)
);

CREATE TABLE mercaderia
(
	idmercaderia int not null auto_increment,
    nombre varchar(45) not null,
    stock int not null,
    precio double not null,
    idtipo int not null,
    
    constraint pk_mercaderia primary key (idmercaderia),
    constraint fk_mercaderia_tipo foreign key (idtipo) references tipo (idtipo)
);

DELIMITER $$
insert into tipo
(nombre)
values
('Electro'),
('Limpieza'),
('Hogar')
$$