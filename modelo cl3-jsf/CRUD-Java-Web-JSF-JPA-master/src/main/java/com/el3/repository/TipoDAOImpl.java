package com.el3.repository;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.el3.entity.Tipo;
import com.el3.utils.JPAUtil;

public class TipoDAOImpl implements TipoDAO {
	
	private SessionFactory sessionFactory;
	
	public TipoDAOImpl() {
		this.sessionFactory = JPAUtil.getSessionFactory();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Tipo> listar() {
		Session session = null;
		String hql="From Tipo";
		List<Tipo> lista = null;
		try {
			session=sessionFactory.openSession();
			Query q=session.createQuery(hql);
			lista=q.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			session.close();			
		}
		return lista;
	}
}
