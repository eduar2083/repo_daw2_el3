package com.el3.controller;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.el3.entity.Tipo;
import com.el3.repository.TipoDAOImpl;

@ManagedBean(name="componenteTipo")
@ViewScoped
public class TipoController {
	private TipoDAOImpl service;
	
	private List<Tipo> lista;
	
	public TipoController() {
		this.service = new TipoDAOImpl();
	}

	public List<Tipo> getLista() {
		lista = service.listar();
		
		return lista;
	}

	public void setLista(List<Tipo> lista) {
		this.lista = lista;
	}
}
