package com.el3.repository;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.el3.entity.Mercaderia;
import com.el3.utils.JPAUtil;

public class MercaderiaDAOImpl implements MercaderiaDAO {
	
	private SessionFactory sessionFactory;
	
	public MercaderiaDAOImpl() {
		this.sessionFactory = JPAUtil.getSessionFactory();
	}

	@Override
	public Mercaderia guardar(Mercaderia bean) {
		Session session = null;
		try {
			session=sessionFactory.openSession();
			session.getTransaction().begin();
			session.save(bean);
			session.getTransaction().commit();
		} catch (Exception e) {
			session.getTransaction().rollback();
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		
		return bean;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Mercaderia> filtrarXPrecio(double minimo, double maximo) {
		Session session = null;
		List<Mercaderia> lista = null;
		try {
			session = sessionFactory.openSession();
			String hql = "select m from Mercaderia m inner join Tipo t on m.tipo = t.idTipo Where m.precio between ?1 and ?2";
			Query q = session.createQuery(hql);
			q.setParameter(1, minimo == -1 ? 0 : minimo);
			q.setParameter(2, maximo == -1 ? Integer.MAX_VALUE : maximo);
			lista = q.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			session.close();			
		}
		return lista;
	}

	@Override
	public void eliminar(int id) {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Mercaderia obj = session.get(Mercaderia.class, id);
			session.getTransaction().begin();
			session.delete(obj);
			session.getTransaction().commit();
		} catch (Exception e) {
			session.getTransaction().rollback();
			e.printStackTrace();
		}
		finally{
			session.close();
		}
	}
}
