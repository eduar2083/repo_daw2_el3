package com.el3.controller;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.component.api.UIData;

import com.el3.entity.Mercaderia;
import com.el3.repository.MercaderiaDAOImpl;
import com.el3.utils.Constantes;

@ManagedBean(name="componenteMercaderia")
@ViewScoped
public class MercaderiaController {
	
	private MercaderiaDAOImpl service;
	
	private List<Mercaderia> lista;
	private Mercaderia mercaderia;
	
	private double precioMinimo;
	private double precioMaximo;
	
	private UIData fila;
	
	public MercaderiaController() {
		this.service = new MercaderiaDAOImpl();
		this.lista = service.filtrarXPrecio(-1, -1);	// Todos
		this.mercaderia = new Mercaderia();
	}

	public List<Mercaderia> getLista() {
		return lista;
	}

	public void setLista(List<Mercaderia> lista) {
		this.lista = lista;
	}

	public Mercaderia getMercaderia() {
		return mercaderia;
	}

	public void setMercaderia(Mercaderia mercaderia) {
		this.mercaderia = mercaderia;
	}

	public double getPrecioMinimo() {
		return precioMinimo;
	}

	public void setPrecioMinimo(double precioMinimo) {
		this.precioMinimo = precioMinimo;
	}

	public double getPrecioMaximo() {
		return precioMaximo;
	}

	public void setPrecioMaximo(double precioMaximo) {
		this.precioMaximo = precioMaximo;
	}
	
	public UIData getFila() {
		return fila;
	}

	public void setFila(UIData fila) {
		this.fila = fila;
	}
	
	// ===============
	
	public void nuevo() {
		this.mercaderia = new Mercaderia();
	}
	
	public void registrar() {
		try {
			Mercaderia m = service.guardar(mercaderia);
			Constantes.mensaje("Sistema", "Registro guardado", FacesMessage.SEVERITY_INFO);
			System.out.println(m);
		}
		catch (Exception e) {
			e.printStackTrace();
			Constantes.mensaje("Sistema", "Error en el registro", FacesMessage.SEVERITY_ERROR);
		}
	}
	
	public void obtener() {
		mercaderia = (Mercaderia) fila.getRowData();
	}
	
	public void eliminar() {
		try {
			service.eliminar(mercaderia.getIdMercaderia());
			Constantes.mensaje("Sistema", "Registro eliminado", FacesMessage.SEVERITY_INFO);
		}
		catch (Exception e) {
			e.printStackTrace();
			Constantes.mensaje("Sistema", "Error en la eliminación", FacesMessage.SEVERITY_ERROR);
		}
	}

	public void filtrar() {
		this.lista = service.filtrarXPrecio(precioMinimo, precioMaximo);
	}
}
