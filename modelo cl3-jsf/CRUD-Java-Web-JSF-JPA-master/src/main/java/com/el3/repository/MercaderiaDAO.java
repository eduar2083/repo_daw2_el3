package com.el3.repository;

import java.util.List;

import com.el3.entity.Mercaderia;

public interface MercaderiaDAO {
	public Mercaderia guardar(Mercaderia bean);
	public List<Mercaderia> filtrarXPrecio(double minimo, double maximo);
	public void eliminar(int id);
}
