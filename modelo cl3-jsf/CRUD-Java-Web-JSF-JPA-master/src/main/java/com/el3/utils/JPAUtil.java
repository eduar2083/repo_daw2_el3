package com.el3.utils;


import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class JPAUtil {
	
	public static SessionFactory getSessionFactory() { 
		SessionFactory sessionFactory= new Configuration().
			configure("/META-INF/hibernate.cfd.xml").buildSessionFactory();
		return sessionFactory;
	}
}
